package Enum;

/**
 * @author Johann van Eeden
 */
public enum Seed {

    EMPTY,
    BLACK,
    BLACK_SHADOW,
    BLACK_SHADOW_REDLINED,
    WHITE,
    WHITE_SHADOW,
    WHITE_SHADOW_REDLINED
}

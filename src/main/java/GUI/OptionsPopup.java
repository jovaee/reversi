package GUI;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 *
 */
public class OptionsPopup extends JDialog {


    public enum Option {
        CANCEL,
        OK
    }

    private JSpinner spinner;

    private Option closeAction = Option.CANCEL;

    public OptionsPopup(int depth) {
        init(depth);
    }

    private void init(int depth) {
        setModal(true);
        setModalityType(ModalityType.APPLICATION_MODAL);

        setTitle("Reversi Options");
        setSize(350, 400);

        spinner = new JSpinner(new SpinnerNumberModel(depth, 1, Integer.MAX_VALUE, 1));

        JLabel label = new JLabel("Not implemented", SwingConstants.CENTER);
        JButton btnOK = new JButton("OK");
        btnOK.addActionListener(e -> {
            closeAction = Option.OK;

            dispose();
        });

        ((JPanel) getContentPane()).setBorder(new EmptyBorder(10, 10, 10 ,10));

        getContentPane().setLayout(new BorderLayout(10, 10));
        getContentPane().add(spinner, BorderLayout.PAGE_START);
        getContentPane().add(label, BorderLayout.CENTER);
        getContentPane().add(btnOK, BorderLayout.PAGE_END);

        setLocationRelativeTo(null);
    }

    public int getDepth() {
        return (int) spinner.getValue();
    }

    public Option getCloseAction() {
        return closeAction;
    }
}

package GUI;

import Enum.Seed;
import Logic.Logic;
import Logic.Solver;
import Logic.State;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Line2D;
import java.awt.geom.RoundRectangle2D;

/**
 * @author Johann van Eeden
 */
public class GameGUI extends JFrame {

    private int NUM_COLS = 8;
    private int NUM_ROWS = 8;

    private final double SIDE_PADDING = 5;

    // Predefined initial cell sizes. Updates when window size changes
    private double CELL_HEIGHT = 75;
    private double CELL_WIDTH = 75;

    // Predefined initial window size. Updates when window size changes
    private int WINDOW_HEIGHT = (int) Math.ceil(CELL_HEIGHT * NUM_ROWS + SIDE_PADDING * 2);
    private int WINDOW_WIDTH = (int) Math.ceil(CELL_WIDTH * NUM_COLS + SIDE_PADDING * 2);

    private final Color COLOR_BACKGROUND = new Color(0, 158, 11);

    private final Color COLOR_BLACK_PLAYER = new Color(0, 0, 0);
    private final Color COLOR_BLACK_PLAYER_SHADOW = new Color(0, 0, 0, 135);
    private final Color COLOR_WHITE_PLAYER = new Color(255, 255, 255);
    private final Color COLOR_WHITE_PLAYER_SHADOW = new Color(255, 255, 255, 135);

    private int REDLINED_SHADOW_LAST_COLUMN = -1;
    private int REDLINED_SHADOW_LAST_ROW = -1;

    private int DEPTH = 1;
    private double DIFFICULTY = 0.6;

    private boolean TMP_PLAYER_ONE = true;

    private Seed[][] board;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
        }

        EventQueue.invokeLater(() -> new GameGUI());
    }

    /**
     * Constructor
     */
    public GameGUI() {
        board = Logic.initBoard(NUM_ROWS, NUM_COLS);

        Logic.getPossibleDrops(board, Seed.BLACK, NUM_ROWS, NUM_COLS);

        init();

//        showStartupPopup();
    }

    private void showStartupPopup() {
        StartupPopup sp = new StartupPopup();
        sp.setVisible(true);
    }

    private void showOptionPopup() {
        OptionsPopup op = new OptionsPopup(DEPTH);
        op.setVisible(true);

        if (op.getCloseAction() == OptionsPopup.Option.OK) {
            DEPTH = op.getDepth();

            System.out.printf("New max search depth set to %d\n", DEPTH);
        }
    }

    /**
     * Cleans the logical board and the GUI.GameGUI board
     */
    private void restartGame() {
        board = Logic.initBoard(NUM_ROWS, NUM_COLS);

        Logic.getPossibleDrops(board, Seed.BLACK, NUM_ROWS, NUM_COLS);

        repaint();

        System.gc();
    }

    /**
     *
     * @param set
     */
    private void setBusyCursor(boolean set) {
        if (set) {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        } else {
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    /**
     * Calculate the size each cell should be according to the current window size
     */
    private void calcCellSizes() {
        CELL_WIDTH = (WINDOW_WIDTH - 2 * SIDE_PADDING) / (double) NUM_COLS;
        CELL_HEIGHT = (WINDOW_HEIGHT - 2 * SIDE_PADDING) / (double) NUM_ROWS;
    }

    /**
     *
     */
    private void init() {
        setTitle("Reversi (Othello)");
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        MenuItem mi1 = new MenuItem("Very Easy");
        mi1.addActionListener(e -> {
            DIFFICULTY = 0.2;

            restartGame();
        });

        MenuItem mi2 = new MenuItem("Easy");
        mi2.addActionListener(e -> {
            DIFFICULTY = 0.4;

            restartGame();
        });

        MenuItem mi3 = new MenuItem("Normal");
        mi3.addActionListener(e -> {
            DIFFICULTY = 0.6;

            restartGame();
        });

        MenuItem mi4 = new MenuItem("Hard");
        mi4.addActionListener(e -> {
            DIFFICULTY = 0.8;

            restartGame();
        });

        MenuItem mi5 = new MenuItem("Very Hard");
        mi5.addActionListener(e -> {
            DIFFICULTY = 1.0;

            restartGame();
        });
        MenuItem miRestart = new MenuItem("Restart Game");
        miRestart.addActionListener(e -> restartGame());

        MenuItem miOption = new MenuItem("Options");
        miOption.addActionListener(e -> showOptionPopup());

        PopupMenu pm = new PopupMenu("Difficulty Setting");
        pm.add(mi1);
        pm.add(mi2);
        pm.add(mi3);
        pm.add(mi4);
        pm.add(mi5);
        pm.add(miRestart);
        pm.add(miOption);

        DrawCanvas canvas = new DrawCanvas();
        canvas.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                super.mouseMoved(e);

                int row = (int) ((e.getY() - 2 * SIDE_PADDING) / CELL_HEIGHT);
                int col = (int) ((e.getX() - 2 * SIDE_PADDING) / CELL_WIDTH);

                if (!(row >= 0 && row < NUM_ROWS && col >= 0 && row < NUM_COLS)) {
                    System.err.printf("mouseMoved(): Invalid index (%d, %d)\n", row, col);

                    return;
                }

                if (TMP_PLAYER_ONE) {
                    if (board[row][col] == Seed.BLACK_SHADOW && (row != REDLINED_SHADOW_LAST_ROW || col != REDLINED_SHADOW_LAST_COLUMN)) {
                        if (REDLINED_SHADOW_LAST_COLUMN != -1) {
                            board[REDLINED_SHADOW_LAST_ROW][REDLINED_SHADOW_LAST_COLUMN] = Seed.BLACK_SHADOW;
                        }

                        REDLINED_SHADOW_LAST_ROW = row;
                        REDLINED_SHADOW_LAST_COLUMN = col;

                        board[row][col] = Seed.BLACK_SHADOW_REDLINED;
                        repaint();
                    } else if (REDLINED_SHADOW_LAST_COLUMN != -1 && (row != REDLINED_SHADOW_LAST_ROW || col != REDLINED_SHADOW_LAST_COLUMN)) {
                        board[REDLINED_SHADOW_LAST_ROW][REDLINED_SHADOW_LAST_COLUMN] = Seed.BLACK_SHADOW;

                        REDLINED_SHADOW_LAST_ROW = -1;
                        REDLINED_SHADOW_LAST_COLUMN = -1;

                        repaint();
                    }
                }/* else {
                    if (board[row][col] == Seed.WHITE_SHADOW && (row != REDLINED_SHADOW_LAST_ROW || col != REDLINED_SHADOW_LAST_COLUMN)) {
                        if (REDLINED_SHADOW_LAST_COLUMN != -1) {
                            board[REDLINED_SHADOW_LAST_ROW][REDLINED_SHADOW_LAST_COLUMN] = Seed.WHITE_SHADOW;
                        }

                        REDLINED_SHADOW_LAST_ROW = row;
                        REDLINED_SHADOW_LAST_COLUMN = col;

                        board[row][col] = Seed.WHITE_SHADOW_REDLINED;
                        repaint();
                    } else if (REDLINED_SHADOW_LAST_COLUMN != -1 && (row != REDLINED_SHADOW_LAST_ROW || col != REDLINED_SHADOW_LAST_COLUMN)) {
                        board[REDLINED_SHADOW_LAST_ROW][REDLINED_SHADOW_LAST_COLUMN] = Seed.WHITE_SHADOW;

                        REDLINED_SHADOW_LAST_ROW = -1;
                        REDLINED_SHADOW_LAST_COLUMN = -1;

                        repaint();
                    }
                }*/
            }
        });
        canvas.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                if (e.getButton() == MouseEvent.BUTTON3) {
                    pm.show(canvas, e.getX(), e.getY());
                } else if (e.getButton() == MouseEvent.BUTTON1) {
                    int tmp;
                    int row = (int) ((e.getY() - 2 * SIDE_PADDING) / CELL_HEIGHT);
                    int col = (int) ((e.getX() - 2 * SIDE_PADDING) / CELL_WIDTH);

//                    System.out.printf("Row: %d\tCol: %d\n", row, col);

                    tmp = Logic.dropTile(board, Seed.BLACK, NUM_ROWS, NUM_COLS, row, col);

                    if (tmp != -1) {

                        REDLINED_SHADOW_LAST_ROW = -1;
                        REDLINED_SHADOW_LAST_COLUMN = -1;

                        Logic.removePossibleDrops(board, NUM_ROWS, NUM_COLS);

                        repaint();

                        if (Logic.isWin(board, Seed.BLACK, NUM_ROWS, NUM_COLS)) {
                            int[] counts = Logic.countColorTiles(board, board.length, board[0].length);
                            JOptionPane.showMessageDialog(null, "Player BLACK has won\nBLACK: " + counts[0] + "\nWHITE: " + counts[1]);
                            restartGame();

                            return;
                        }

                        // CPU player move

                        new Thread(() -> {
                            setBusyCursor(true);

                            System.out.println("CPU MOVE");
                            State apple = Solver.solve(board, DEPTH, DIFFICULTY);

                            if (apple.rowAdded < 0 || apple.colAdded < 0) {
                                JOptionPane.showMessageDialog(null, "NFGJSDHFKJSDBHF DGHFUIYSDF&&D^F&DTFI*");
                                restartGame();
                            } else {
                                board = apple.board;
                                Logic.getPossibleDrops(board, Seed.BLACK, NUM_ROWS, NUM_COLS);

                                System.out.printf("AI placed tile in Row: %d\tCol: %d\n", apple.rowAdded, apple.colAdded);

                                SwingUtilities.invokeLater(() -> repaint());

                                setBusyCursor(false);

                                if (Logic.isWin(board, Seed.WHITE, NUM_ROWS, NUM_COLS)) {
                                    int[] counts = Logic.countColorTiles(board, board.length, board[0].length);
                                    JOptionPane.showMessageDialog(null, "Player WHITE has won\nBLACK: " + counts[0] + "\nWHITE: " + counts[1]);
                                    restartGame();
                                }
                            }
                        }).start();
                    }
                }
            }
        });
        canvas.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);

                WINDOW_WIDTH = e.getComponent().getWidth();
                WINDOW_HEIGHT = e.getComponent().getHeight();

                calcCellSizes();
                repaint();
            }
        });

        canvas.add(pm);

        add(canvas);

        setVisible(true);
        setLocationRelativeTo(null);
    }

    private class DrawCanvas extends JPanel {

        @Override
        public void paint(Graphics g) {
            super.paint(g);

            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            setBackground(COLOR_BACKGROUND);

            g2.setStroke(new BasicStroke(2.0f));

            // Draw horizontal borders
            for (int i = 0; i < NUM_ROWS + 1; i = i + 1) {
                g2.draw(new Line2D.Double(SIDE_PADDING, i * CELL_HEIGHT + SIDE_PADDING, WINDOW_WIDTH - SIDE_PADDING, i * CELL_HEIGHT + SIDE_PADDING));
            }

            // Draw vertical borders
            for (int i = 0; i < NUM_COLS + 1; i = i + 1) {
                g2.draw(new Line2D.Double(i * CELL_WIDTH + SIDE_PADDING, SIDE_PADDING, i * CELL_WIDTH + SIDE_PADDING, WINDOW_HEIGHT - SIDE_PADDING));
            }

            // Draw seeds
            for (int i = 0; i < NUM_ROWS; i = i + 1) {
                for (int j = 0; j < NUM_COLS; j = j + 1) {
                    if (board[i][j] == Seed.BLACK) {
                        g2.setColor(COLOR_BLACK_PLAYER);
                    } else if (board[i][j] == Seed.WHITE) {
                        g2.setColor(COLOR_WHITE_PLAYER);
                    } else if (board[i][j] == Seed.WHITE_SHADOW || board[i][j] == Seed.WHITE_SHADOW_REDLINED) {
                        g2.setColor(COLOR_WHITE_PLAYER_SHADOW);
                    } else if (board[i][j] == Seed.BLACK_SHADOW || board[i][j] == Seed.BLACK_SHADOW_REDLINED) {
                        g2.setColor(COLOR_BLACK_PLAYER_SHADOW);
                    }

                    if (board[i][j] != Seed.EMPTY) {
                        g2.setStroke(new BasicStroke(1.0f));
                        g2.fill(new RoundRectangle2D.Double((j * CELL_WIDTH + CELL_WIDTH * 0.05) + SIDE_PADDING, i * CELL_HEIGHT + CELL_HEIGHT * 0.05 + SIDE_PADDING, CELL_WIDTH * 0.9, CELL_HEIGHT * 0.9, 150, 150));
                    }

                    if (board[i][j] == Seed.BLACK_SHADOW_REDLINED || board[i][j] == Seed.WHITE_SHADOW_REDLINED) {
                        g2.setColor(Color.RED);
                        g2.setStroke(new BasicStroke(1.5f));
                        g2.draw(new RoundRectangle2D.Double((j * CELL_WIDTH + CELL_WIDTH * 0.05) + SIDE_PADDING, i * CELL_HEIGHT + CELL_HEIGHT * 0.05 + SIDE_PADDING, CELL_WIDTH * 0.9, CELL_HEIGHT * 0.9, 150, 150));
                    }
                }
            }
        }
    }
}

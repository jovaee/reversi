package Logic;

import Enum.Seed;

/**
 *
 */
public class Logic {

    /**
     * Create an empty board with all cells initialized as <i>EMPTY</i> expect the middle four tiles
     */
    public static Seed[][] initBoard(int numRows, int numCols) {
        Seed[][] board = new Seed[numRows][numCols];

        for (int i = 0; i < numRows; i = i + 1) {
            for (int j = 0; j < numCols; j = j + 1) {
                board[i][j] = Seed.EMPTY;
            }
        }

        board[3][3] = Seed.WHITE;
        board[4][4] = Seed.WHITE;
        board[4][3] = Seed.BLACK;
        board[3][4] = Seed.BLACK;

        return board;
    }

    @SuppressWarnings("Duplicates")
    private static boolean lookRowRight(Seed[][] board, Seed seed, Seed opponentSeed, int row, int numRows, int col, int numCols) {

        int opponentCount = 0;

        int tmpRow = -1;
        int tmpCol = -1;

        for (int i = col + 1; i < numCols && board[row][i] == opponentSeed; i = i + 1) {
            if (board[row][i] == opponentSeed) {
                opponentCount += 1;
            }

            tmpRow = row;
            tmpCol = i;
        }

        return tmpCol < numCols - 1 && opponentCount > 0 && board[tmpRow][tmpCol + 1] == seed;
    }

    @SuppressWarnings("Duplicates")
    private static boolean lookRowLeft(Seed[][] board, Seed seed, Seed opponentSeed, int row, int numRows, int col, int numCols) {

        int opponentCount = 0;

        int tmpRow = -1;
        int tmpCol = -1;

        for (int i = col - 1; i >= 0 && board[row][i] == opponentSeed; i = i - 1) {
            if (board[row][i] == opponentSeed) {
                opponentCount += 1;
            }

            tmpRow = row;
            tmpCol = i;
        }

        return tmpCol > 0 && opponentCount > 0 && board[tmpRow][tmpCol - 1] == seed;
    }

    @SuppressWarnings("Duplicates")
    private static boolean lookColumnDown(Seed[][] board, Seed seed, Seed opponentSeed, int row, int numRows, int col, int numCols) {

        int opponentCount = 0;

        int tmpRow = -1;
        int tmpCol = -1;

        for (int i = row + 1; i < numRows && board[i][col] == opponentSeed; i = i + 1) {
            if (board[i][col] == opponentSeed) {
                opponentCount += 1;
            }

            tmpRow = i;
            tmpCol = col;
        }

        return tmpRow < numRows - 1 && opponentCount > 0 && board[tmpRow + 1][tmpCol] == seed;
    }

    @SuppressWarnings("Duplicates")
    private static boolean lookColumnUp(Seed[][] board, Seed seed, Seed opponentSeed, int row, int numRows, int col, int numCols) {

        int opponentCount = 0;

        int tmpRow = -1;
        int tmpCol = -1;

        for (int i = row - 1; i >= 0 && board[i][col] == opponentSeed; i = i - 1) {
            if (board[i][col] == opponentSeed) {
                opponentCount += 1;
            }

            tmpRow = i;
            tmpCol = col;
        }

        return tmpRow > 0 && opponentCount > 0 && board[tmpRow - 1][tmpCol] == seed;
    }

    @SuppressWarnings("Duplicates")
    private static boolean lookLeftRightDown(Seed[][] board, Seed seed, Seed opponentSeed, int row, int numRows, int col, int numCols) {

        int opponentCount = 0;

        int tmpRow = -1;
        int tmpCol = -1;

        for (int i = row + 1, j = col + 1; i < numRows && j < numCols && board[i][j] == opponentSeed; i = i + 1, j = j + 1) {
            if (board[i][j] == opponentSeed) {
                opponentCount += 1;
            }

            tmpRow = i;
            tmpCol = j;
        }

        return tmpRow < numRows - 1 && tmpCol < numCols - 1 && opponentCount > 0 && board[tmpRow + 1][tmpCol + 1] == seed;
    }

    @SuppressWarnings("Duplicates")
    private static boolean lookLeftRightUp(Seed[][] board, Seed seed, Seed opponentSeed, int row, int numRows, int col, int numCols) {

        int opponentCount = 0;

        int tmpRow = -1;
        int tmpCol = -1;

        for (int i = row - 1, j = col + 1; i >= 0 && j < numCols && board[i][j] == opponentSeed; i = i - 1, j = j + 1) {
            if (board[i][j] == opponentSeed) {
                opponentCount += 1;
            }

            tmpRow = i;
            tmpCol = j;
        }

        return tmpRow > 0 && tmpCol < numCols - 1 && opponentCount > 0 && board[tmpRow - 1][tmpCol + 1] == seed;

    }

    @SuppressWarnings("Duplicates")
    private static boolean lookRightLeftDown(Seed[][] board, Seed seed, Seed opponentSeed, int row, int numRows, int col, int numCols) {

        int opponentCount = 0;

        int tmpRow = -1;
        int tmpCol = -1;

        for (int i = row + 1, j = col - 1; i < numRows && j >= 0 && board[i][j] == opponentSeed; i = i + 1, j = j - 1) {
            if (board[i][j] == opponentSeed) {
                opponentCount += 1;
            }

            tmpRow = i;
            tmpCol = j;
        }

        return tmpRow < numRows - 1 && tmpCol > 0 && opponentCount > 0 && board[tmpRow + 1][tmpCol - 1] == seed;
    }

    @SuppressWarnings("Duplicates")
    private static boolean lookRightLeftUp(Seed[][] board, Seed seed, Seed opponentSeed, int row, int numRows, int col, int numCols) {

        int opponentCount = 0;

        int tmpRow = -1;
        int tmpCol = -1;

        for (int i = row - 1, j = col - 1; i >= 0 && j >= 0 && board[i][j] == opponentSeed; i = i - 1, j = j - 1) {
            if (board[i][j] == opponentSeed) {
                opponentCount += 1;
            }

            tmpRow = i;
            tmpCol = j;
        }

        return tmpRow > 0 && tmpCol > 0 && opponentCount > 0 && board[tmpRow - 1][tmpCol - 1] == seed;
    }

    /**
     *
     * @param board
     * @param seed
     * @param row
     * @param numRows
     * @param col
     * @param numCols
     * @return
     */
    private static boolean surroundCheck(Seed[][] board, Seed seed, int row, int numRows, int col, int numCols) {

        Seed opponentSeed;

        if (seed == Seed.BLACK) {
            opponentSeed = Seed.WHITE;
        } else {
            opponentSeed = Seed.BLACK;
        }

        return lookRowRight(board, seed, opponentSeed, row, numRows, col, numCols) || lookRowLeft(board, seed, opponentSeed, row, numRows, col, numCols) ||
                lookColumnDown(board, seed, opponentSeed, row, numRows, col, numCols) || lookColumnUp(board, seed, opponentSeed, row, numRows, col, numCols) ||
                lookLeftRightDown(board, seed, opponentSeed, row, numRows, col, numCols) || lookLeftRightUp(board, seed, opponentSeed, row, numRows, col, numCols) ||
                lookRightLeftDown(board, seed, opponentSeed, row, numRows, col, numCols) || lookRightLeftUp(board, seed, opponentSeed, row, numRows, col, numCols);
    }

    /**
     *
     * @param board
     * @param seed
     * @param numRows
     * @param numCols
     */
    public static void getPossibleDrops(Seed[][] board, Seed seed, int numRows, int numCols) {

        for (int i = 0; i < numRows; i = i + 1) {
            for (int j = 0; j < numCols; j = j + 1) {
                if (board[i][j] == Seed.EMPTY && seed == Seed.WHITE && surroundCheck(board, Seed.WHITE, i, numRows, j, numCols)) {
                    board[i][j] = Seed.WHITE_SHADOW;
                } else if (board[i][j] == Seed.EMPTY && seed == Seed.BLACK && surroundCheck(board, Seed.BLACK, i, numRows, j, numCols)) {
                    board[i][j] = Seed.BLACK_SHADOW;
                }
            }
        }
    }

    /**
     *
     * @param board
     * @param numRows
     * @param numCols
     */
    public static void removePossibleDrops(Seed[][] board, int numRows, int numCols) {

        for (int i = 0; i < numRows; i = i + 1) {
            for (int j = 0; j < numCols; j = j + 1) {
                if (board[i][j] != Seed.BLACK && board[i][j] != Seed.WHITE && board[i][j] != Seed.EMPTY) {
                    board[i][j] = Seed.EMPTY;
                }
            }
        }
    }

    /**
     *
     * @param board
     * @return
     */
    public static Seed[][] copyBoard(Seed[][] board) {
        Seed[][] newBoard = new Seed[board.length][board[0].length];

        for (int i = 0; i < board.length; i = i + 1) {
            for (int j = 0; j < board[0].length; j = j + 1) {
                newBoard[i][j] = board[i][j];
            }
        }

        return newBoard;
    }

    /**
     * @param board
     * @param seed The player dropping the tile
     * @param numRows The number of rows on the board
     * @param numCols The number of columns on the board
     * @param row The row the tile should be placed in
     * @param col The column the tile should be place in
     * @return The number of tiles that was turned over for Seed <i>seed</i>
     */
    public static int dropTile(Seed[][] board, Seed seed, int numRows, int numCols, int row, int col) {

        Seed[][] boardCopy;
        Seed opponentSeed;

        int count = 1;

        if (board[row][col] == Seed.WHITE || board[row][col] == Seed.BLACK || board[row][col] == Seed.EMPTY) {
//            System.err.println("The column is already full");

            return -1;
        }

        if (seed == Seed.BLACK) {
            opponentSeed = Seed.WHITE;
        } else {
            opponentSeed = Seed.BLACK;
        }

        removePossibleDrops(board, numRows, numCols);

        boardCopy = copyBoard(board);

        // Change the said tile to the set seed
        board[row][col] = seed;

        if (lookRowRight(boardCopy, seed, opponentSeed, row, numRows, col, numCols)) {
            // Flip all tiles that must be in the row (right)
            for (int i = col + 1; i < numCols && board[row][i] == opponentSeed; i = i + 1) {
                if (board[row][i] == opponentSeed) {
                    board[row][i] = seed;
                    count += 1;
                }
            }
        }

        if (lookRowLeft(boardCopy, seed, opponentSeed, row, numRows, col, numCols)) {
            // Flip all tiles that must be in the row (left)
            for (int i = col - 1; i >= 0 && board[row][i] == opponentSeed; i = i - 1) {
                if (board[row][i] == opponentSeed) {
                    board[row][i] = seed;
                    count += 1;
                }
            }
        }

        if (lookColumnDown(boardCopy, seed, opponentSeed, row, numRows, col, numCols)) {
            // Flip all tiles that must be in the col (down)
            for (int i = row + 1; i < numRows && board[i][col] == opponentSeed; i = i + 1) {
                if (board[i][col] == opponentSeed) {
                    board[i][col] = seed;
                    count += 1;
                }
            }
        }

        if (lookColumnUp(boardCopy, seed, opponentSeed, row, numRows, col, numCols)) {
            // Flip all tiles that must be in the col (up)
            for (int i = row - 1; i >= 0 && board[i][col] == opponentSeed; i = i - 1) {
                if (board[i][col] == opponentSeed) {
                    board[i][col] = seed;
                    count += 1;
                }
            }
        }

        if (lookLeftRightDown(boardCopy, seed, opponentSeed, row, numRows, col, numCols)) {
            // Flip all tiles that must be in the left to right diagonal (down)
            for (int i = row + 1, j = col + 1; i < numRows && j < numCols && board[i][j] == opponentSeed; i = i + 1, j = j + 1) {
                if (board[i][j] == opponentSeed) {
                    board[i][j] = seed;
                    count += 1;
                }
            }
        }

        if (lookLeftRightUp(boardCopy, seed, opponentSeed, row, numRows, col, numCols)) {
            // Flip all tiles that must be in the left to right diagonal (up)
            for (int i = row - 1, j = col + 1; i >= 0 && j < numCols && board[i][j] == opponentSeed; i = i - 1, j = j + 1) {
                if (board[i][j] == opponentSeed) {
                    board[i][j] = seed;
                    count += 1;
                }
            }
        }

        if (lookRightLeftDown(boardCopy, seed, opponentSeed, row, numRows, col, numCols)) {
            // Flip all tiles that must be in the right to left diagonal (down)
            for (int i = row + 1, j = col - 1; i < numRows && j >= 0 && board[i][j] == opponentSeed; i = i + 1, j = j - 1) {
                if (board[i][j] == opponentSeed) {
                    board[i][j] = seed;
                    count += 1;
                }
            }
        }

        if (lookRightLeftUp(boardCopy, seed, opponentSeed, row, numRows, col, numCols)) {
            // Flip all tiles that must be in the right to left diagonal (up)
            for (int i = row - 1, j = col - 1; i >= 0 && j >= 0 && board[i][j] == opponentSeed; i = i - 1, j = j - 1) {
                if (board[i][j] == opponentSeed) {
                    board[i][j] = seed;
                    count += 1;
                }
            }
        }

        return count;
    }

    /**
     *
     * @param board
     * @param numRows
     * @param numCols
     * @return
     */
    private static boolean isMovePossible(Seed[][] board, int numRows, int numCols) {

        for (int i = 0; i < numRows; i = i + 1) {
            for (int j = 0; j < numCols; j = j + 1) {
                if (board[i][j] != Seed.BLACK && board[i][j] != Seed.WHITE) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     *
     * @param board
     * @param numRows
     * @param numCols
     * @return
     */
    public static int[] countColorTiles(Seed[][] board, int numRows, int numCols) {

        int numWhites = 0;
        int numBlacks = 0;

        for (int i = 0; i < numRows; i = i + 1) {
            for (int j = 0; j < numCols; j = j + 1) {
                if (board[i][j] == Seed.BLACK) {
                    numBlacks += 1;
                } else if (board[i][j] == Seed.WHITE) {
                    numWhites += 1;
                }
            }
        }

        return new int[] {numBlacks, numWhites};
    }

    /**
     *
     * @param board
     * @param numRows
     * @param numCols
     */
    public static void printBoard(Seed[][] board, int numRows, int numCols) {

        for (int i = 0; i < numRows; i = i + 1) {
            for (int j = 0; j < numCols; j = j + 1) {
                System.out.printf("%s ", board[i][j]);
            }

            System.out.println();
        }
    }

    /**
     * Check if the player with the tile at <i>row</i> and <i>col</i> is a winner
     *
     * @param player
     * @return True is it is a win otherwise False
     */
    public static boolean isWin(Seed[][] board, Seed player, int numRows, int numCols) {
        if (isMovePossible(board, numRows, numCols)) {
            return false;
        }

        int[] counts = countColorTiles(board, numRows, numCols);

        if (player == Seed.BLACK && counts[0] > counts[1]) {
            return true;
        } else if (player == Seed.WHITE && counts[1] > counts[0]) {
            return true;
        }

        return false;
    }

}

package Logic;

import java.util.*;

import Enum.Seed;

/**
 * @author Johann van Eeden
 */
public class Solver {

    // The max tree depth
    private static int MAX_DEPTH = 9;

    // Should alpha-beta pruning be used
    private static final boolean SHOULD_PRUNE = true;

    /**
     *
     * @param board
     * @param depth
     *@param diff  @return
     */
    public static State solve(Seed[][] board, int depth, double diff) {
        MAX_DEPTH = depth;

        System.out.println("-----------------------------");

        State parent = new State(null, board, -1, -1);

        makeWhiteMoves(parent, 0, Integer.MIN_VALUE, Integer.MAX_VALUE);

        parent = findMaxMove(parent);
//        int[] counts = Logic.countColorTiles(parent.board, parent.board.length, parent.board[0].length);

//        System.out.printf("BLACK : %3d\tWHITE : %3d\n", counts[0], counts[1]);

        return getFirstState(parent);
    }

    /**
     *
     * @param root
     * @return
     */
    private static State findMaxMove(State root) {

        State maxState = null, tmp;
        int max = Integer.MIN_VALUE;

        if (root.getChildren().size() == 0) {
            return root;
        }

        for (State s : root.getChildren()) {
            tmp = findMinMove(s);

            if (getHeuristic(tmp) > max) {

                max = getHeuristic(tmp);
//                System.out.println("New Max Node: " + max);
                maxState = tmp;
            }
        }

        root.getChildren().clear();

        return maxState;
    }

    /**
     *
     * @param root
     * @return
     */
    private static State findMinMove(State root) {

        State minState = null, tmp;
        int min = Integer.MAX_VALUE;

        if (root.getChildren().size() == 0) {
            return root;
        }

        for (State s : root.getChildren()) {
            tmp = findMaxMove(s);

            if (getHeuristic(tmp) < min) {
//                System.out.println("New Min Node");

                min = getHeuristic(tmp);
                minState = tmp;
            }
        }

        root.getChildren().clear();

        return minState;
    }

    /**
     *
     * @param board
     * @param diff
     * @return
     */
    private static boolean randomMoveMade(Seed[][] board, double diff) {
        int row;
        int col;

        Random r = new Random();

//        if (1.0 - diff >= r.nextDouble()) {

//            do {
//                col = r.nextInt(board[0].length);
//            } while ((row = Logic.dropTile(board, Seed.BLACK, board.length, , , col)) == -1);

//            bestWhiteWin = new State(null, Seed.BLACK, board, row, col, 0);

//            return true;
//        }

        return false;
    }

    /**
     *
     * @param state
     * @return
     */
    private static int getHeuristic(State state) {

        int[] counts = Logic.countColorTiles(state.board, state.board.length, state.board[0].length);

        return counts[1] - counts[0];
    }

    /**
     *
     * @param state
     * @return
     */
    public static State getFirstState(State state) {

        while (state.parent != null && state.parent.parent != null) {
            state = state.parent;
        }

        return state;
    }

    /**
     *  @param state
     * @param depth
     * @param alpha
     * @param beta
     */
    private static int makeBlackMoves(State state, int depth, int alpha, int beta) {

        Seed[][] currentBoard = state.board;
        Seed[][] tmp;

        State child;

        int v = Integer.MAX_VALUE;

        if (depth == MAX_DEPTH || Logic.isWin(currentBoard, Seed.WHITE, currentBoard.length, currentBoard[0].length)) {
            return getHeuristic(state);
        }

        Logic.getPossibleDrops(currentBoard, Seed.BLACK, currentBoard.length, currentBoard[0].length);

        for (int i = 0; i < currentBoard.length; i = i + 1) {
            for (int j = 0; j < currentBoard[0].length; j = j + 1) {
                if (currentBoard[i][j] == Seed.BLACK_SHADOW) {
                    tmp = Logic.copyBoard(currentBoard);

                    Logic.dropTile(tmp, Seed.BLACK, currentBoard.length, currentBoard[0].length, i, j);
                    Logic.removePossibleDrops(tmp, currentBoard.length, currentBoard[0].length);

                    child = new State(state, tmp, i, j);
                    state.addChild(child);

                    v = Math.min(v, makeWhiteMoves(child, depth + 1, alpha, beta));
                    beta = Math.min(v, beta);
                    if (beta <= alpha && SHOULD_PRUNE) {
                        Logic.removePossibleDrops(currentBoard, currentBoard.length, currentBoard[0].length);

                        return v;
                    }
                }
            }
        }

        Logic.removePossibleDrops(currentBoard, currentBoard.length, currentBoard[0].length);

        return v;
    }

    /**
     * @param state
     * @param depth
     * @param alpha
     * @param beta
     */
    private static int makeWhiteMoves(State state, int depth, int alpha, int beta) {

        Seed[][] currentBoard = state.board;
        Seed[][] tmp;

        State child;

        int v = Integer.MIN_VALUE;

        if (depth == MAX_DEPTH || Logic.isWin(currentBoard, Seed.BLACK, currentBoard.length, currentBoard[0].length)) {
            return getHeuristic(state);
        }

        Logic.getPossibleDrops(currentBoard, Seed.WHITE, currentBoard.length, currentBoard[0].length);

        for (int i = 0; i < currentBoard.length; i = i + 1) {
            for (int j = 0; j < currentBoard[0].length; j = j + 1) {
                if (currentBoard[i][j] == Seed.WHITE_SHADOW) {
                    tmp = Logic.copyBoard(currentBoard);

                    Logic.dropTile(tmp, Seed.WHITE, currentBoard.length, currentBoard[0].length, i, j);
                    Logic.removePossibleDrops(tmp, currentBoard.length, currentBoard[0].length);

                    child = new State(state, tmp, i, j);
                    state.addChild(child);

                    v = Math.max(v, makeBlackMoves(child, depth + 1, alpha, beta));
                    alpha = Math.max(v, alpha);
                    if (beta <= alpha && SHOULD_PRUNE) {
                        Logic.removePossibleDrops(currentBoard, currentBoard.length, currentBoard[0].length);

                        return v;
                    }
                }
            }
        }

        Logic.removePossibleDrops(currentBoard, currentBoard.length, currentBoard[0].length);

        return v;
    }
}

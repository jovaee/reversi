package Logic;

import Enum.Seed;

import java.util.LinkedList;
import java.util.List;

public class State {

    public final State parent;

    public final Seed[][] board;

    public final int rowAdded;
    public final int colAdded;

    private List<State> children = new LinkedList<State>();

    public State(State parent, Seed[][] board, int rowAdded, int colAdded) {
        this.parent = parent;

        this.board = board;

        this.rowAdded = rowAdded;
        this.colAdded = colAdded;
    }

    public void addChild(State child) {
        children.add(child);
    }

    public List<State> getChildren() {
        return children;
    }
}
